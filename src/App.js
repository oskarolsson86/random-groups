import React, { useState } from 'react';
import './App.css';

function App() {
  const [names] = useState(['Oskar', 'Sven', 'Karl', 'Carl', 'Tora', 'Angelina', 'Kristoffer', 'Joakim', 'Emil', 'Martin', 'Jakob', 'Erik', 'Timothy', 'Jessica', 'Andreas']);
  const [groupAmount, setGroupAmount] = useState(0);
  const [groups, setGroups] = useState([]);
  const [groupsGenerated, setGroupsGenerated] = useState(false);
 
  const generateGroups = () => {
    let copyNames = [...names];
    for(let i = copyNames.length - 1; i > 0; i--){
      const j = Math.floor(Math.random() * i)
      const temp = copyNames[i]
      copyNames[i] = copyNames[j]
      copyNames[j] = temp
    }
    
    let groupArr = [];
    let groupObj = [];
   for (let index = 0; index < groupAmount; index++) {
     console.log( names.length / groupAmount );
     let roundup = names.length / groupAmount;

     let cool = Math.round(roundup)
     
    groupObj = copyNames.splice(0, cool )
     groupArr.push(groupObj);
   }
   setGroups(groupArr)
   
    
    setGroupsGenerated(true);
  }
  
  return (
    <div className="App">
      <label>Amount of groups</label>
     <input type="number" value={groupAmount} onChange={e => setGroupAmount(e.target.value)}/>
       
      <button onClick={generateGroups}>Generate Groups</button>
      {groupsGenerated ? 
      groups.map((group, i ) => {
       return <div key={i}>
          <div style={{height:'50px'}}> <p style={{marginBottom: 0}}>Grupp {i} </p> 
          {group.join(' ')}
          </div> </div>
      })
      
      : null}
    </div>
  );
}

export default App;
